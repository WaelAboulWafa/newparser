﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CVApp
{
    class Program
    {
            

        static void Main(string[] args)
        {


            var theip = ConfigurationManager.AppSettings["IP"].ToString();
            var theport = ConfigurationManager.AppSettings["Port"].ToString();
            var conpath = ConfigurationManager.AppSettings["contextPath"].ToString();


           


            string thefilename_args = "";

            if (args.Length < 1)
            {
                Console.WriteLine("No Argument(s) supplied . Please enter file name as argument.");
                Console.WriteLine("Example C:\\> CVApp.exe filename.pdf");
                return;
            }

            thefilename_args = args[0].Trim();
          
          //  Console.WriteLine(args.Length.ToString());

            /*
            for(int i=0;i< args.Length;i++)
            {

            }
            */


            string thefilename = System.IO.Path.GetFileName(thefilename_args);
            thefilename = HttpUtility.UrlEncode(thefilename);



            Byte[] filebytes = File.ReadAllBytes(thefilename_args);
            String file_64 = Convert.ToBase64String(filebytes);
            file_64 = HttpUtility.UrlEncode(file_64);


            string theurl = "http://" + theip + ":" + theport + conpath;


            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("----------------------------");
            Console.WriteLine("----------REQUEST-----------");
            Console.WriteLine("----------------------------");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");



            Console.WriteLine(theurl);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(theurl);
            request.Method = "POST";

            request.ContentType = "application/x-www-form-urlencoded";
            string postData = "filename=" + thefilename + "&base64=" + file_64;



            Console.WriteLine(postData);

            byte[] bytes = Encoding.UTF8.GetBytes(postData);
            request.ContentLength = bytes.Length;


            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);

            WebResponse response = request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream);

            var result = reader.ReadToEnd();

            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("-----------------------------");
            Console.WriteLine("----------RESPONSE-----------");
            Console.WriteLine("-----------------------------");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");


            Console.WriteLine(result);

            stream.Dispose();
            reader.Dispose();



            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("---------PARSING RESPONSE------------");
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");



            var json_returned = JsonConvert.DeserializeObject<dynamic>(result);

            var surename = "";
            try { surename = json_returned.basics.name.surname; } catch (Exception e) { surename = ""; }
            Console.WriteLine("surename: " + surename);


            var firstname = "";
            try { firstname = json_returned.basics.name.firstName; } catch (Exception e) { firstname = ""; }
            Console.WriteLine("forname: " + firstname);


            var JobTitle = "";
            try { JobTitle = json_returned.basics.title; } catch (Exception e) { JobTitle = ""; }
            Console.WriteLine("JobTitle: " + JobTitle);


            var theday = "";
            try { theday = json_returned.basics.avail.day; } catch (Exception e) { theday = ""; }
            Console.WriteLine("availday: " + theday);

            var themon = "";
            try { themon = json_returned.basics.avail.mon; } catch (Exception e) { themon = ""; }
            Console.WriteLine("availmon: " + themon);

            var theyear = "";
            try { theyear = json_returned.basics.avail.year; } catch (Exception e) { theyear = ""; }
            Console.WriteLine("availyear: " + theyear);


            var dobday = "";
            try { dobday = json_returned.basics.dob.day; } catch (Exception e) { dobday = ""; }
            Console.WriteLine("dobday: " + dobday);

            var dobmon = "";
            try { dobmon = json_returned.basics.dob.mon; } catch (Exception e) { dobmon = ""; }
            Console.WriteLine("dobmon: " + dobmon);

            var dobyear = "";
            try { dobyear = json_returned.basics.dob.year; } catch (Exception e) { dobyear = ""; }
            Console.WriteLine("dobyear: " + dobyear);

            var age = "";
            try { age = json_returned.basics.age; } catch (Exception e) { age = ""; }
            Console.WriteLine("age: " + age);

            var hometel = "";
            try { hometel = json_returned.basics.phone[0]; } catch (Exception e) { hometel = ""; }
            Console.WriteLine("hometel: " + hometel);

            var worktel = "";
            try { worktel = json_returned.basics.phone[1]; } catch (Exception e) { worktel = ""; }
            Console.WriteLine("worktel: " + worktel);


            var mobile = "";
            try { mobile = json_returned.basics.phone[2]; } catch (Exception e) { mobile = ""; }
            Console.WriteLine("mobile: " + mobile);


            var email = "";
            try { email = json_returned.basics.email[0]; } catch (Exception e) { email = ""; }
            Console.WriteLine("email: " + email);

            var postal = "";
            try { postal = json_returned.basics.postal; } catch (Exception e) { postal = ""; }
            Console.WriteLine("postal: " + postal);


            var Address = "";
            try { Address = json_returned.basics.address[0]; } catch (Exception e) { Address = ""; }
            Console.WriteLine("Address: " + Address);



            var HouseNBR = "";
            try { HouseNBR = json_returned.basics.HouseNBR; } catch (Exception e) { HouseNBR = ""; }
            Console.WriteLine("HouseNBR: " + HouseNBR);



            var Street = "";
            try { Street = json_returned.basics.Street; } catch (Exception e) { Street = ""; }
            Console.WriteLine("Street: " + Street);



            var City = "";
            try { City = json_returned.basics.City; } catch (Exception e) { City = ""; }
            Console.WriteLine("City: " + City);



            var SubUrb = "";
            try { SubUrb = json_returned.basics.SubUrb; } catch (Exception e) { SubUrb = ""; }
            Console.WriteLine("SubUrb: " + SubUrb);


            var County = "";
            try { County = json_returned.basics.County; } catch (Exception e) { County = ""; }
            Console.WriteLine("County: " + County);

            var Country = "";
            try { Country = json_returned.basics.Country; } catch (Exception e) { Country = ""; }
            Console.WriteLine("Country: " + Country);


            var married = "";
            try { married = json_returned.basics.married; } catch (Exception e) { married = ""; }
            Console.WriteLine("married: " + married);


            var gender = "";
            try { gender = json_returned.basics.gender; } catch (Exception e) { gender = ""; }

            if (gender == "male")
            {
                Console.WriteLine("female: False");
                Console.WriteLine("male: True" );
            }
            if (gender == "female")
            {
                Console.WriteLine("female: True");
                Console.WriteLine("male: False");
            }


      /*      var male = "";
            try { male = json_returned.basics.male; } catch (Exception e) { male = ""; }
            Console.WriteLine("male: " + male);
      */




        }




    }
}
